/*************************************************************************
    @ File Name: t_title_token.cpp
    @ Method:
    @ Author: Jerry Shi
    @ Mail: jerryshi0110@gmail.com 
    @ Created Time: 2015年09月14日 星期一 20时35分15秒
 ************************************************************************/
#include <iostream>
#include <vector>
#include <fstream>
#include <boost/progress.hpp>
#include <boost/unordered_map.hpp>
#include "../src/horse_tokenize.h"


using namespace std;

int main()
{
	cout << "Start Process...\n";
	knlp::HorseTokenize* tok;
	string pth = "/work/mproj/workproj/github/token-dict/dict";
	tok = new knlp::HorseTokenize(pth);

	string target = "keywords-korea.dict";
	string data = "title-korea.txt";
	
	vector<pair<string,float> > segTerms;
	boost::unordered_map<string,int> keywords;
	boost::unordered_map<string,int>::iterator it;

	ifstream in;
	in.open(data.c_str());
	if(!in)
		cerr << "Open source data error!\n";
	ofstream out;
	out.open(target.c_str(),ios::app);
	if(!out)
		cerr << "Open target file error!\n";

	string line;
	while(getline(in,line))
	{
		if(line.size() == 0)
			continue;
		segTerms.clear();

		try
		{
			tok->tokenize(line,segTerms);
		}
		catch(...)
		{
			segTerms.clear();
		}
		for(int i = 0; i < segTerms.size();++i)
		{
			it = keywords.find(segTerms[i].first);
			if(it == keywords.end())
				keywords.insert(make_pair(segTerms[i].first,1));
		}
	}
	in.close();

	for(it = keywords.begin();it != keywords.end();++it)
		out << it->first << "\n";
	out.close();

	return 0;
	
}

//complie commend
//g++ filename -o output -lboost_system

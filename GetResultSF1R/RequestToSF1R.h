/*************************************************************************
    @ File Name: RequestToSF1R.h
    @ Method:
    @ Author: Jerry Shi
    @ Mail: jerryshi0110@gmail.com 
    @ Created Time: 2015年09月15日 星期二 15时16分51秒
 ************************************************************************/
#include <iostream>
#include <curl/curl.h>
#include <string>
#include <fstream>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/progress.hpp>
#include "json/json.h"
#include <set>


std::size_t writeCallBack(char* str,std::size_t size,std::size_t nmemb,void* up);

class RequestToSF1
{
	public:
		RequestToSF1();
		~RequestToSF1();

		bool PostToSF1( const std::string& query);
		void GetAttrs(int& label_num
				,std::string& res
				,int& total_count);
		void GetResNum(int& ResCount);
		void GetJsonRes(const std::string& query
				,int& Topk
				,std::string& JsonRes
				,int& total_count);
		void Flush(const std::string& filename);
		//std::size_t writeCallBack(char* str,std::size_t size,std::size_t nmemb
		//		,void* up);
	private:
		std::string gResponseData_;
		std::string PostData_;
		std::string SF1Url_;
};

RequestToSF1::RequestToSF1(){
	PostData_ = "{\"analyzer_result\":\"true\",\"attr\":{\"attr_result\":true,\"attr_top\":20},\"collection\":\"b5mq\",\"header\":{\"check_time\":\"true\"},\"limit\":20,\"offset\":0,\"remove_duplicated_result\":\"true\",\"search\":{\"analyzer\":{\"apply_la\":\"true\",\"use_original_keyword\":false,\"use_synonym_extension\":\"true\"},\"keywords\":\"term\",\"log_keywords\":\"false\",\"searching_mode\":{\"mode\":\"zambezi\"}},\"sort\":[{\"order\":\"DESC\",\"property\":\"SalesAmount\"}]}";
	gResponseData_ = "";
	
	//online server 
	SF1Url_ = "http://10.30.96.188:8888/sf1r/documents/search";
}

RequestToSF1::~RequestToSF1()
{
}
std::string gResponseData;
std::size_t writeCallBack(char* str
		,std::size_t size,std::size_t nmemb,void* up){
	//gResponseData = "";
	for(int i = 0; i < size * nmemb; ++i)
		gResponseData.push_back(str[i]);

//	std::cout << "test post-4:" << gResponseData << std::endl;	
	return size * nmemb;
}

bool RequestToSF1::PostToSF1(const std::string& query){
	if(query.empty())
		return false;
	gResponseData = "";
	Json::Reader PostReader;
	Json::FastWriter fwriter;
	Json::Value SourceJson;
	if(!PostReader.parse(PostData_.c_str(),SourceJson,false))
		return false;

	//set the searching keywords
	SourceJson["search"]["keywords"] = Json::Value(query.c_str());

	//serialization to string
	std::string post = fwriter.write(SourceJson);

	//curl 
	try {
		CURL* curl;
		CURLcode ret;

		struct curl_slist* headers = NULL;
		headers = curl_slist_append(headers,"Accept: application/json");
		headers = curl_slist_append(headers,"Content-Type: application/json");
		headers = curl_slist_append(headers,"charsets: utf-8");

		curl_global_init(CURL_GLOBAL_ALL);
		curl = curl_easy_init();

		if(!curl){
			return false;
		}

		//post data
		curl_easy_setopt(curl,CURLOPT_POSTFIELDS,post.c_str());
		curl_easy_setopt(curl,CURLOPT_POSTFIELDSIZE,post.length());

		//url
		curl_easy_setopt(curl,CURLOPT_URL,SF1Url_.c_str());
		curl_easy_setopt(curl,CURLOPT_CUSTOMREQUEST,"POST");
		curl_easy_setopt(curl,CURLOPT_HTTPHEADER,headers);

		//timeout
		curl_easy_setopt(curl,CURLOPT_TIMEOUT,5);
		curl_easy_setopt(curl,CURLOPT_FOLLOWLOCATION,1);

		//callback
		curl_easy_setopt(curl,CURLOPT_WRITEFUNCTION,&writeCallBack);

		//perform
		ret = curl_easy_perform(curl);

		curl_easy_cleanup(curl);
		curl_global_cleanup();
	} catch(std::exception& ex) {
		std::cout << "curl expection: " << ex.what() << std::endl;
	}
	return true;
}

void RequestToSF1::GetAttrs(int& label_nm
		,std::string& Res
		,int& total_count) {
	if(gResponseData.empty())
		return;

	Json::Reader reader;
	Json::Value AttrJson;
	if(!reader.parse(gResponseData,AttrJson,false))
		return;
	if(AttrJson["attr"].size() == 0)
		return;
	Res ="[";
	std::string sRes;
	//get attribute value
	for(int i = 0; i <= label_nm; ++i){
		Json::Value val = AttrJson["attr"][i];
		if(val["attr_name"].asString() == "KNN")
			continue;
		//std::cout << "+++++:" << val["attr_name"].asString() << std::endl;
		int upbound = val["labels"].size() < label_nm ? val["labels"].size() : label_nm;
		sRes ="[";
		std::set<std::string> labelList;
		for(int j = 0; j < upbound; ++j) {
			Json::Value labelVal = val["labels"][j];
			std::string s = labelVal["label"].asString();
			int pos = s.find("/");
			s = s.substr(0,pos);
			//std::vector<std::string> vec;
			//boost::split(vec,s,boost::is_any_of("/"));
			if(labelList.insert(s).second)
				sRes += "\"" + s + "\",";
		}
		labelList.clear();
		if(*sRes.rbegin() == ',')
			sRes.erase(sRes.size() - 1);
		sRes += "],";
		if(sRes != "[],")
			Res += sRes;
	}
	if(*Res.rbegin() == ',')
		Res.erase(Res.size() - 1);
	Res += "]";
	//std::cout << "----:" << Res << std::endl;
	
	total_count = AttrJson["total_count"].asInt();
	//std::cout << "count1:" << total_count << std::endl;
	gResponseData = "";	
}

void RequestToSF1::GetResNum(int& ResCount) {
	Json::Reader reader;
	Json::Value response;
	if(!reader.parse(gResponseData,response,false))
		ResCount = 0;
	ResCount = response["total_count"].asInt();
}

void RequestToSF1::GetJsonRes(const std::string& query
		,int& Topk
		,std::string& JsonRes
		,int& total_count){
	JsonRes = "";
	if(query.empty())
		JsonRes = "[]";

	PostToSF1(query);

	std::string res;
	
	GetAttrs(Topk,res,total_count);
	JsonRes += query + "\t";
	JsonRes += res;
}

void RequestToSF1::Flush(const std::string& filename){
	
	std::ifstream fin(filename.c_str());
	if(!fin){
		std::cout << "Failed to open " << filename << std::endl;
		return;
	}

	//get total line number
	std::size_t totalLines = 0;
	std::string line;
	while(getline(fin,line))
		++totalLines;

	boost::progress_display show_progress(totalLines);
	fin.close(); //set the location to read
	fin.open(filename.c_str());
	std::string res;
	int total_count;
	std::ofstream fout("terms.attrs",std::iostream::app);
	if(!fout){
		std::cout << "Failed to open terms.attrs\n"; 
	}
	int topk = 5;
	while(getline(fin,line)){
		if(line.empty())
			continue;
		int pos = line.find("\t");
		if(pos == std::string::npos)
			continue;
		std::string term = line.substr(0,pos);
		GetJsonRes(term,topk,res,total_count);
		if(res.empty())
			continue;
		fout << res << "\t" << total_count << std::endl;
		++show_progress;
	}
	fout.close();
	fin.close();
}

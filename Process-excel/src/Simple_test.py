#!/usr/bin/python

# -*- coding: utf8 -*-
import sys,xlrd

#open excel and get data
def open_excel(file='xf_class.xls'):
    try:
        data = xlrd.open_workbook(file)
        return data
    except Exception,e:
        print str(e)

#get data by index
def excel_table_byindex(file='xf_class.xls',colnameindex=0,by_index=0):
    data = open_excel(file)
    table = data.sheets()[by_index]
    nrows = table.nrows # row number
    ncols = table.ncols # col number
    colnames = table.row_values(colnameindex)

    list = []
    for rownum in range(1,nrows):
        row = table.row_values(rownum)
        if row:
            app = {}
            for i in range(len(colnames)):
                app[colnames[i]] = row[i]
            list.append(app)
    return list

#main

def main():
    tables = excel_table_byindex()
    for row in tables:
        print row

if __name__ == "__main__":
    main()

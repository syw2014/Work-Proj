/*************************************************************************
    @ File Name: t_GetTaoBaoData.cpp
    @ Method:
    @ Author: Jerry Shi
    @ Mail: jerryshi0110@gmail.com 
    @ Created Time: 2015年09月07日 星期一 10时33分04秒
 ************************************************************************/
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <fstream>
#include <curl/curl.h>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/functional/hash.hpp>
#include <boost/progress.hpp>

using namespace std;
size_t WriteToStr(char *str,size_t size, size_t nmemb,void* userp);
size_t ResponseData(char* buf,size_t size,size_t nmemb,void* userp);

boost::hash<std::string> hash_query;

//string s = "";
//get
bool getUrl(char* filename)
{
	CURL *curl;
	CURLcode res;
	FILE *fp;
	
	// store data in file
	if((fp = fopen(filename,"w")) == NULL)
		return false;

	struct curl_slist *headers = NULL;
	headers = curl_slist_append(headers,"Accept: Agent-007");
	//initial curl
	curl = curl_easy_init();

	if(curl)
	{
		curl_easy_setopt(curl,CURLOPT_HTTPHEADER,headers); // modify headers
		curl_easy_setopt(curl,CURLOPT_URL,"http://www.baidu.com"); //set url
		curl_easy_setopt(curl,CURLOPT_WRITEDATA,fp); //output return headers to *fp
		curl_easy_setopt(curl,CURLOPT_HEADERDATA,fp);// return html data to *fp
		res = curl_easy_perform(curl); // run
	}

	if(res != 0) //success
	{
		curl_slist_free_all(headers);
		curl_easy_cleanup(curl);
	}

	fclose(fp);

	return true;
}

//post
bool postUrl(string& UserQuery)
{
	CURL *curl;
	CURLcode res;
	
	//struct curl_slist *headers = NULL;
	//headers = curl_slist_append(headers,"Accept: application/json");
	curl = curl_easy_init();
	string query = "q=";
	query += UserQuery;

	if(curl)
	{
		//curl_easy_setopt(curl,CURLOPT_HTTPHEADER,headers);
		curl_easy_setopt(curl,CURLOPT_POSTFIELDS,query.c_str());// set post data
		curl_easy_setopt(curl,CURLOPT_URL,"http://s.taobao.com/search"); //set url
		curl_easy_setopt(curl,CURLOPT_WRITEFUNCTION,ResponseData);
		//curl_easy_setopt(curl,CURLOPT_WRITEDATA,fp); // wirte data in file

		//
		res = curl_easy_perform(curl);
		curl_easy_cleanup(curl);
	}


	return true;
}

string gResponseData;

size_t ResponseData(char* buf,size_t size,size_t nmemb,void* userp)
{
	for(int i = 0; i< size*nmemb; ++i)
		gResponseData.push_back(buf[i]);

	return size*nmemb;
}

//callback
size_t WriteToStr(char* str,size_t size,size_t nmemb,void* userp)
{
	string ss;

	for(int i = 0; i < size*nmemb;++i)
		ss.push_back(str[i]);
//	FILE* fptr = (FILE*)userp;
	//fwrite(str,size,nmemb,fptr);
//	fprintf(fptr,"%s",str);
	vector<string> v;

	ss = str;
	//cout << ss << endl;
	int pos1 = ss.find("rsKeywords");
	int pos2 = ss.find("],\"rewriteStatus");
	if(pos1 != string::npos && pos2 != string::npos)
	{
		string s1 = ss.substr(pos1+13,pos2-pos1-13);
		string s2;
		int pos3 = s1.find("],\"allOldBiz30Day");
		
		s2 = s1.substr(0,pos3);
		if(s2.size() != 0)
		{
		boost::split(v,s2,boost::is_any_of(","));
		ofstream Out,OutV;
		Out.open("HotRskeywords.txt",ios::app);
		OutV.open("HotRskeywords.v",ios::app);
		if(!Out.is_open())
			cerr << "Open output file error!\n";
		if(!OutV.is_open())
			cerr << "Open dictionary file error!\n";
		if(v.size() < 2)
			return size * nmemb;

		//size_t hs = hash_query(s);
		//cout << "hash" << hs << endl;
		//OutV << hs << "\t";
		for(int i = 0; i < v.size();++i)
		{
			boost::trim_if(v[i],boost::is_any_of("\""));
			//cout << v[i] << ",";
			Out << v[i] << "\t";
			OutV << v[i] << "\t";
		}
		Out << endl;
		OutV << endl;
		OutV.close();
		Out.close();
		}
	}

	return size*nmemb;
}

int main(int argc ,char** argv)
{
	if(argc != 2)
	{
		cout << "Usage: " << argv[0] << "source_filepath\n";
		return -1;
	}
	size_t total_line = 3001;
	boost::progress_display show_progress(total_line);
	string File = argv[1];
	size_t Found_pos = File.rfind('/');
	size_t FileName_pos1 = (Found_pos == string::npos)? 0 :(Found_pos +1);
	Found_pos = File.rfind('.');
    size_t FileName_pos2 = (Found_pos == string::npos) ? File.size() : Found_pos;
	string OutFile = File.substr(FileName_pos1,(FileName_pos2 - FileName_pos1));

	boost::posix_time::ptime Tstart,Tend;
	boost::posix_time::millisec_posix_time_system_config::time_duration_type Telapse;
	Tstart = boost::posix_time::microsec_clock::universal_time();
	ifstream in;
	in.open(File.c_str());
	if(!in.is_open())
		cerr << "open hotkeywords file error!\n";

	ofstream outHash;
	string File_hash = string("../corpus/rskeywords/"+OutFile+"_rskeywords.v");
	outHash.open(File_hash.c_str(),ios::app);
	if(!outHash.is_open())
		cerr << "Open out hash file error!\n";

	ofstream outTxt;
	string File_txt = string("../corpus/rskeywords/"+OutFile+"_rskeywords.corp");
	outTxt.open(File_txt.c_str(),ios::app);
	if(!outTxt.is_open())
		cerr << "Open out txt file error!\n";

	vector<string> v;
	string s;
	while(getline(in,s))
	{
		if(s.size() == 0)
			continue;
	postUrl(s);
	v.clear();

	/**/
	size_t pos1 = gResponseData.find("rsKeywords");
	size_t pos2 = gResponseData.find("],\"rewriteStatus");
	if(pos1 != string::npos && pos2 != string::npos)
	{
		string s1 = gResponseData.substr(pos1+13,pos2-pos1-13);
		size_t pos3 = s1.find("],\"allOldBiz30Day");
		string s2 = s1.substr(0,pos3);
		if(s2.size() != 0)
		{
		boost::split(v,s2,boost::is_any_of(","));
		if(v.size() < 2)
			continue;
		size_t hs = hash_query(s);
		outHash << hs << "\t";
		outTxt << s << "\t";
		for(int i = 0; i < v.size(); ++i)
		{
			boost::trim_if(v[i],boost::is_any_of("\""));
		//	cout << v[i] << ",";
			outHash << v[i] << "\t";
			outTxt << v[i] << "\t";
		}
		//cout << '\n';
		outHash << endl;
		outTxt << endl;
		}
	}
	gResponseData = "";
	//sleep(1);
	Tend = boost::posix_time::microsec_clock::universal_time();
	Telapse = Tend - Tstart;
	++show_progress;
	//cout << "Process time :" << (float)Telapse.ticks() / 1000 << "ms\n";
	}

	outHash.close();
	outTxt.close();
	in.close();
	return 0;
}


//complie commend
//g++ t_GetTaoBaoData.cpp -o t_GetTaBaoData -lcurl

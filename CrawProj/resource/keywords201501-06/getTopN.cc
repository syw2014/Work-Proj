/*************************************************************************
    @ File Name: getTopN.cc
    @ Method:
    @ Author: Jerry Shi
    @ Mail: jerryshi0110@gmail.com 
    @ Created Time: 2015年09月17日 星期四 14时06分25秒
 ************************************************************************/
#include <iostream>
#include <fstream>
#include <vector>
#include <boost/algorithm/string.hpp>

using namespace std;

int main(int argc,char** argv)
{
	if(argc !=2)
	{
		cout << "Usage: No source file path!\n";
		return -1;
	}

	string file = argv[1];
	ifstream in;
	in.open(file.c_str());
	if(!in)
		cerr << "open error!\n";
	ofstream out;
	out.open("KeywordsTop3K.txt",ios::app);
	if(!out)
		cerr << "open error !\n";
	string line;
	int cnt = 0;
	vector<string> v;
	while(getline(in,line))
	{
		if(line.size() > 31)
			continue;
		v.clear();
		if(cnt < 3001)
		{
			cnt += 1;
			boost::split(v,line,boost::is_any_of("\t"));
			if(v.size() < 2)
				continue;
			out << v[0] << endl;
		}
	}

	in.close();
	out.close();

	cout << "Process done!\n";
	return 0;
}


/*************************************************************************
    @ File Name: tokenizeTerm.cpp
    @ Method:input a file ,output file contain terms
    @ Author: Jerry Shi
    @ Mail: jerryshi0110@gmail.com 
    @ Created Time: 2015年09月25日 星期五 16时29分25秒
 ************************************************************************/
#include <iostream>
#include <fstream>
#include <vector>
#include <boost/progress.hpp>
#include <boost/unordered_map.hpp>

#include "horse_tokenize.h"

using namespace std;

size_t getTotalLine(const char* filename){
	if(filename == NULL)
		return 0;
	ifstream in;
	in.open(filename,ios::in);
	if(!in){
		cout << "Open " << filename << "error \n";
	   return 0;	
	}
	string line;
	size_t count;
	while(getline(in,line)){
		count += 1;
	}
	in.close();
	
	return count;
}

int main(int argc,char** argv)
{
	if(argc < 2){
		cout << "Usage: no file need to be process\n";
		return -1;
	}
	
	string filename = argv[1];
	size_t count = getTotalLine(filename.c_str());
	
	boost::progress_display show_progress(count);
	knlp::HorseTokenize* tok_;
	string dict_dir = "/work/mproj/workproj/github/token-dict/dict";
	tok_ = new knlp::HorseTokenize(dict_dir);

	ifstream in;
	in.open(filename.c_str(),ios::in);
	if(!in){
		cout << "Open " << filename << "error!\n";
		return -1;
	}
	
	size_t rpos = filename.rfind('/');
	size_t pos1 = (rpos == string::npos) ? 0 : (rpos+1);
	rpos = filename.rfind('.');
	size_t pos2 = (rpos == string::npos) ? filename.size():rpos;
	string outfile = filename.substr(pos1,(pos2-pos1));
	outfile += "_terms.dict";

	ofstream out;
	out.open(outfile.c_str(),ios::out);
	if(!out) {
		cout << "Open " << outfile << "error!\n";
		return -1;
	}

	string line;
	vector<pair<string,float> > segterms;
	vector<string> v;

	while(getline(in,line)) {
		if(line.length() == 0)
			continue;
		v.clear();
		boost::split(v,line,boost::is_any_of("\t"));
		if(v[0].size() == 0)
			continue;
		segterms.clear();
		try {
			tok_->tokenize(v[0],segterms);
		}
		catch(...) {
			segterms.clear();
		}

		for(int i = 0; i < segterms.size(); ++i)
			out << segterms[i].first << endl;
		++show_progress;
	}	

	in.close();
	out.close();

	return 0;
}


//g++ tokenizeTerm.cpp -o tokenizeTerm -I /work/mproj/workproj/github/Work-Proj/wordsSegment/src -lboost_system

#########################################################################
# File Name: attr_info.sh
# Method:get attribute informations of keyword
# Author: JerryShi
# mail: jerryshi0110@gmail.com
# Created Time: 2015年10月13日 星期二 15时56分16秒
#########################################################################
#!/bin/bash

path=$(dirname $0)
cd $path

args=("$@")
filename=${args[0]}

if [ ! -e "$filename" ]
then
	echo "Usage: input file is not exist!"
	exit 0
fi


sf1r_info="sf1_info.txt"
ss="[]"
if [ -e "$path/$sf1r_info" ]
then
	rm "$path/$sf1r_info"
fi

echo "Sart get informations form sf1!"

while read line
do
	#echo $line

	term=`echo -e "$line" | cut -d $'\t' -f1`
	#echo "term: $term"
	freq=`echo -e "$line" | cut -d $'\t' -f2`
	#echo "freq: $freq"
	
	#filter 
	if [ $freq -le 30 ]
	then
		continue;
	fi

	#json data to sf1
	data="{\"analyzer_result\":\"true\",\"attr\":{\"attr_result\":true,\"attr_top\":20},\"collection\":\"b5mq\",\"header\":{\"check_time\":\"true\"},\"limit\":20,\"offset\":0,\"remove_duplicated_result\":\"true\",\"search\":{\"analyzer\":{\"apply_la\":\"true\",\"use_original_keyword\":false,\"use_synonym_extension\":\"true\"},\"keywords\":\"$term\",\"log_keywords\":\"false\",\"searching_mode\":{\"mode\":\"zambezi\"}},\"sort\":[{\"order\":\"DESC\",\"property\":\"SalesAmount\"}]}"

	#online search engine:10.30.97.188,product environment: 10.30.96.188
	wget -q --post-data="$data" http://10.30.97.188:8888/sf1r/documents/search -O "$sf1r_info"

#	curl -d $data http://10.30.97.188:8888/sf1r/documents/search > "$sf1r_info"

	#print keyword without results
	while [ ! -e "$sf1r_info" ]
	do
		echo "--- --- No searching results of term :$term"
	done

	attrs=`python ./extractAttr.py -f $sf1r_info`
	if [ $attrs != $ss ]
	then
		echo "$term	$attrs" >> terms_info.txt
	fi

	#extract attributes
done < $filename
echo "Process complete!"




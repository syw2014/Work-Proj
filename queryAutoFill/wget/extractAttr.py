#!/usr/bin/env python

########
# Method: extract attributes from given string
# Author: JerryShi
#   Mail: jerryshi@gmail.com
#   Date: 2015.10.14 15:25:30

import sys,getopt

intput = ""
opts, args =  getopt.getopt(sys.argv[1:],"f:")
for op, value in opts:
    if op == '-f':
        input = value
    else:
        print("-f searching result files")
        sys.exit()


#extract lables from given string
def get_labels(attr, label_num):
    pos = 0

    #find attr_name
    name_head = "\"attr_name\":\""
    name_head_len = len(name_head)
    pos = attr.find(name_head)
    if (pos == -1):
        return "[]"
    end_pos = attr.find("\"",pos+name_head_len)
    if(end_pos == -1):
        return "[]"
    #extract att_name
    attr_name = attr[pos+name_head_len : end_pos]
    if(attr_name == "KNN"):
        return "[]"

    #get lable/attribute value
    label_name = "\"label\":\""
    label_name_len = len(label_name)
    label_count = 0
    label_list = []
    s = "["
    while (1):
        pos = attr.find(label_name,pos)
        if(pos == -1):
            break
        else:
            end_pos = attr.find("\"",pos+label_name_len)
            label_value = attr[pos+label_name_len : end_pos]
            #find "/"
            slash_pos = label_value.find("/")
            #find "/" and only get the first one
            if (slash_pos != -1):
                label_value = label_value[0: slash_pos]

            #check this label_value is exist in list
            exist = False
            for lab in label_list:
                if (lab == label_value):
                    exist = True
                    break
            if(exist == False):
                label_list.append(label_value)
                s += "\"" + label_value + "\","
                label_count += 1
            
            #termial condition: label value number > label number(given)
            if(label_count >= label_num):
                break
            pos = end_pos
    #remove the last ','
    if (s[-1] == ','):
        s = s[0:-1]
    s += "]"
    
    return s

#main
result = "["
name_head = "\"attr_name\":\""
name_head_len = len(name_head)
attr_num = 5

sf1r_info = open(input,'r')
for line in sf1r_info:
    if(line == ""):
        continue
    attr_count = 0
    pos1 = line.find(name_head)
    pos2 = 0
    while(1):
        if(pos1 == -1):
            break
        pos2 = line.find(name_head,pos1+name_head_len)
        #print ("test>>> %s") % line[pos1:pos2]
        s = get_labels(line[pos1:pos2],5)
        if(s != "[]" and s != ""):
            result += s + ","
            attr_count += 1
        #only get the topN attribute
        if(attr_count >= attr_num):
            break
        pos1 = pos2

if(len(result) > 0):
    if(result[-1] == ","):
        result = result[0:-1]

result += "]"

print result


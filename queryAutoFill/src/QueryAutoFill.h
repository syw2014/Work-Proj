/*************************************************************************
    @ File Name: QueryAutoFill.h
    @ Method:
    @ Author: Jerry Shi
    @ Mail: jerryshi0110@gmail.com 
    @ Created Time: 2015年10月08日 星期四 13时50分19秒
 ************************************************************************/
#ifndef QUERYAUTOFILL_H
#define QUERYAUTOFILL_H

#include "common.h"
#include "knlp/horse_tokenize.h"
#include "idmlib/query-correction/cn_query_correction.h"
class GreaterBySecond
{
	public:
		bool operator() (const std::pair<int,float>&lh,const std::pair<int,float>& rh) {
			if(lh.second > rh.second)
				return true;
			else
				return false;
		}
};

class QueryAutoFill
{
	public:
		QueryAutoFill(const std::string& pinyin_res_pth,const std::string& dict_pth);
		~QueryAutoFill();
		bool Build(const std::string& terms_file);
		bool Flush(const std::string& terms_file,const std::string& key_terms_file);
		bool Generate(const std::string& term,const unsigned int& termid
				,boost::unordered_map<std::string,vector< std::pair<int,float > > >& key_info,const int len = 10); // generate

		bool Parse(const std::string& str
				,vector<std::string>& chars
				,vector<std::string>& words);  // extract chars and words from string
		bool GenerateByPrefix(const vector<std::string>& chars
				,vector<std::string>& keys
				,const int len);
		bool GenerateByWordInfix(vector<std::string>& words
				,vector<std::string>& keys
				,const int len);
		bool GenerateByWordSuffix(vector<std::string>& words
				,vector<std::string>& keys
				,const int len);
		bool GenerateByPinYinPrefix(const std::string& str
				,vector<std::string>& keys
				,const int len);
		bool GenerateByShengMuPrefix(const std::string& str
				,vector<std::string>& keys
				,const int len);

	private:
		void GetShengMuByPinYin(const vector<std::string>& pinyin
				,vector<std::string>& shm);

	public:
		ilplib::knlp::HorseTokenize* tok_;
		idmlib::qc::CnQueryCorrection* cngr_;
		
		std::vector<std::string> terms_; 
		std::vector<float> term_freq_;
		boost::unordered_map<std::string, std::vector<int> > key_termids_;
		
		std::set<std::string> shm_;

	private:
		static const float prefix_w = 10000.0;
		static const float pinyin_prefix_w = 1000.0;
		static const float shengmu_w = 100.0;
		static const float word_infix_w = 10.0;
		static const float word_suffix_w = 1.0;

		static const unsigned int topN = 15;


};

#endif // QueryAutoFill.h

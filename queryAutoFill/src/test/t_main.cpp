/*************************************************************************
    @ File Name: t_main.cpp
    @ Method:
    @ Author: Jerry Shi
    @ Mail: jerryshi0110@gmail.com 
    @ Created Time: 2015年10月20日 星期二 14时49分08秒
 ************************************************************************/
#include "QueryIndex.h"

using namespace std;

int main(int argc,char* argv[])
{
	/*if(argc != 2)
	{
		cout << "Usage: No keyword to input!\n";
		return 0;
	}*/
	string res_pth = "/work/mproj/workproj/github/token-dict/nlp-corpus/queryLog/autofill-res";
//	string keyFile = res_pth + "/key_terms.txt";
//	string attrFile = res_pth + "/term.attr";
//	string cateFile = res_pth + "/term.cate";
	string keyword = "";
	string res = "";
	cout << "Start Loading...\n";
	QueryIndex qs(res_pth);
	cout << "Dictionary loaded!\nPlease input keyword:\n";
	while(cin >> keyword)
	{
		qs.GetJsonRes(keyword,res);
		cout << "AutoFillResult: " << res << endl;		
		cout << "Please input keyword:\n";
	}

	return 0;
}

//complie commends:
//g++ t_main.cpp -o t_main -I ../ -lboost_system -lboost_filesystem

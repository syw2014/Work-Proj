/*************************************************************************
    @ File Name: t_autofill.cpp
    @ Method:
    @ Author: Jerry Shi
    @ Mail: jerryshi0110@gmail.com 
    @ Created Time: 2015年10月12日 星期一 15时25分12秒
 ************************************************************************/
#include <iostream>
#include "QueryAutoFill.h"

int main(int argc,char* argv[])
{
	if(argc < 2)
	{
		std::cout << "Usage: no query file input!\n";
		return -1;
	}
	std::string keywords = argv[1];
	std::cout << "Start process...\n";
	std::string pinyin = "../../resource/cn";
	std::string dict = "/work/mproj/workproj/github/token-dict/dict/";
	QueryAutoFill* qa = new QueryAutoFill(pinyin,dict);
//	std::string keywords = "/work/mproj/workproj/github/token-dict/nlp-corpus/CorpusForQS/query.freq";
//	std::string keywords = "/work/mproj/workproj/github/token-dict/nlp-corpus/queryLog/log201412-201503/query.freq";
	qa->Build(keywords);
	qa->Flush("terms.txt","key_terms.txt");
	std::cout << "End process!\n";

	delete qa;

	return 0;
}


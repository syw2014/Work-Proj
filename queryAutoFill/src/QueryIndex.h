/*************************************************************************
    @ File Name: QueryIndex.cpp
    @ Method:
    @ Author: Jerry Shi
    @ Mail: jerryshi0110@gmail.com 
    @ Created Time: 2015年10月19日 星期一 16时01分31秒
 ************************************************************************/
#include "common.h"

class QueryIndex
{
	public:
		QueryIndex(const std::string& res_dir);
		~QueryIndex();
		bool GetSuggestion(const std::string& key
				,std::vector<std::string>& terms
				,std::vector<std::string>& attrs
				,std::vector<std::string>& cnts
				,std::string& cate);
		void GetJsonRes(const std::string& key,std::string& jsonRes);
		void SetCates(std::string& category);

	private:
		std::vector<std::string> SplitByPattern(const std::string& str
				,const std::string& tok);
		std::string Normalize(const std::string& input);

	private:
		boost::unordered_map<std::string,std::vector<unsigned int> > maps_;
		std::vector<std::string> attrs_;
		std::vector<std::string> rescnts_;
		std::vector<std::string> terms_;
		std::set<std::string> korean_;
		boost::unordered_map<std::string,std::string> cates_;
		std::string SetCategory_;
};

QueryIndex::QueryIndex(const std::string& res_dir)
:SetCategory_("\"韩国城\","){
	
	if(!boost::filesystem::exists(res_dir))
	{
		std::cerr << "Resource path not exists!\n";
	}
	std::string filename = res_dir + "/key_terms.txt";
	//load prefixes in key terms file
	std::ifstream fin(filename.c_str());
	if(!fin){
		std::cout << "failed to open " << filename << std::endl;
		return;
	}
	boost::unordered_map<std::string,unsigned int> term_id;
	
	std::string line;
	while(getline(fin,line)) {
		if(line.empty())
			continue;
		std::vector<std::string> words = SplitByPattern(line,"\t");
		if(words.size() <  2)
			continue;
		std::vector<unsigned int> ids; //assign id
		//the first one is prefix
		for(unsigned int i = 1; i < words.size(); ++i) {
			boost::unordered_map<std::string,unsigned int>::iterator ite;
			ite = term_id.find(words[i]);
			if(ite != term_id.end()) {
				ids.push_back(ite->second);
			} else {
				terms_.push_back(words[i]);
				unsigned int size = term_id.size();
				term_id[words[i]] = size;
				ids.push_back(size);
			}
		}
		maps_[words[0]].swap(ids);
	}
	fin.close();
	std::cout << "terms_ size:" << terms_.size() <<  std::endl;
	std::cout << "maps_ size:" << maps_.size() <<  std::endl;
	//load attributes
	filename = res_dir + "/term.attr";
	fin.open(filename.c_str());
	if(!fin) {
		std::cout << "Failed to open " << filename << std::endl;
		return;
	}
	attrs_.resize(term_id.size());
	rescnts_.resize(term_id.size());
	while(getline(fin,line)) {
		if(line.empty())
			continue;
/*
		std::string::size_type pos = line.find("\t");
		if(pos == std::string::npos)
			continue;
		std::string term = line.substr(0,pos);
		std::string sline = line.substr(pos+1);
		pos = sline.find("\t");
		std::string attr = line.substr(0,pos);
		std::string count = sline.substr(pos+1);
*/
		std::vector<std::string> vec;
		boost::split(vec,line,boost::is_any_of("\t"));
		if(vec.size() != 3)
			continue;
		if(vec[0].empty() || vec[1].empty())
			continue;
		boost::unordered_map<std::string,unsigned int>::iterator ite;
		ite = term_id.find(vec[0]);
		if(ite != term_id.end()) {
			attrs_[ite->second] = vec[1]; //attributes
			rescnts_[ite->second] = vec[2]; //searching results number
		}
	}
	fin.close();
	std::cout << "attrs_ size:" << attrs_.size() <<  std::endl;
	std::cout << "rescnt_ size:" << rescnts_.size() <<  std::endl;

	//load category
	filename = res_dir + "/term.cate";
	fin.open(filename.c_str());
	if(!fin) {
		std::cout << "Failed to open " << filename << std::endl;
		return;
	}
	while(getline(fin,line)) {
		if(line.empty())
			continue;
		std::string::size_type pos = line.find("\t");
		if(pos == std::string::npos)
			continue;
		std::string term = line.substr(0,pos);
		std::string cate = line.substr(pos+1);
		
		if(term.empty() || cate.empty())
			continue;
		cates_.insert(make_pair(term,cate));
	}
	fin.close();
	term_id.clear();
	std::cout << "cates_ size:" << cates_.size() <<  std::endl;

	//load korean keywords
	filename = res_dir + "/keywords.korea";
	fin.open(filename.c_str());
	while(getline(fin,line)) {
		if(line.empty())
			continue;
		korean_.insert(line);
	}
	fin.close();
	std::cout << "korean_ size: " << korean_.size() << std::endl;
}

QueryIndex::~QueryIndex()
{
}

void QueryIndex::SetCates(std::string& category) {
	if(!category.empty())
		SetCategory_ = category;
}
std::vector<std::string> QueryIndex::SplitByPattern(const std::string& str
		,const std::string& tok) {
	if(str.empty())
		return std::vector<std::string>();
	if(tok.empty())
		return std::vector<std::string>(1,str);

	std::vector<std::string> vect;
	std::string::size_type start_pos = 0;
	for(unsigned int i = 0; i < str.size();++i) {
		if(tok.find(str[i]) != std::string::npos) {
			unsigned int len = i - start_pos;
			if(len > 0)
				vect.push_back(str.substr(start_pos,len));
			start_pos = i + 1;
		}
	}
	//last
	if(start_pos < str.size()) {
		vect.push_back(str.substr(start_pos));
	}

	return vect;
}

std::string QueryIndex::Normalize(const std::string& input ) {
	unsigned int i = 0;
	for(;i < input.size();++i) {
		if(input[i] != ' ' && input[i] != '\t')
			break;
	}

	unsigned int end = input.size();
	for(;end > 0; --end) {
		if(input[end-1] != ' ' && input[end -1] != '\t')
			break;
	}

	//remove more space
	std::string str;
	unsigned int stat = 0;
	for(;i < end; ++i) {
		if(input[i] == ' ' || input[i] == '\t')
			++stat;
		else
			stat = 0;
		if(stat == 0)
			str += input[i];
		else if(stat == 1)
			str += ' ';
	}
	boost::to_lower(str);
	return str;
}
bool QueryIndex::GetSuggestion(const std::string& key
		,std::vector<std::string>& terms
		,std::vector<std::string>& attrs
		,std::vector<std::string>& cnts
		,std::string& cate) {
	std::string NormKey = Normalize(key);
	terms.clear();
	boost::unordered_map<std::string,std::vector<unsigned int> >::iterator ite;
	ite = maps_.find(NormKey);
	if(ite == maps_.end())
		return false;

	std::vector<unsigned int>& ids = ite->second;
	for(unsigned int i = 0; i < ids.size(); ++i) {
		terms.push_back(terms_[ids[i]]);
		attrs.push_back(attrs_[ids[i]]);
		cnts.push_back(rescnts_[ids[i]]);
	}
	boost::unordered_map<std::string,std::string>::iterator it;
	it = cates_.find(NormKey);
	if(it != cates_.end())
		cate = it->second;
	std::set<std::string>::iterator id = korean_.find(NormKey);
	if(id != korean_.end() && !cate.empty()) {
		std::string::size_type pos = cate.find_first_of("[");
		if(pos != std::string::npos && !SetCategory_.empty()) {
			cate.insert(pos+1,SetCategory_);
		}
	}
	return true;
}

void QueryIndex::GetJsonRes(const std::string& key,std::string& jsonRes) {
	jsonRes.clear();
	std::vector<std::string> terms;
	std::vector<std::string> attrs;
	std::vector<std::string> cnts;
	std::string cate;
	
	GetSuggestion(key,terms,attrs,cnts,cate);
	
	jsonRes = "[";
	//
	if(terms.empty()) {
		jsonRes = "]";
		return;
	}

	if(!cate.empty() && cate != "[]")
		jsonRes += "{\"term\":\"" + key + "\",\"cate\":" + cate + "},";
	for(unsigned int i = 0 ; i < terms.size(); ++i) {
		jsonRes += "{\"term\":\"" + terms[i] + "\",\"attr\":";
		if(!attrs[i].empty()) {
			jsonRes += attrs[i];
		} else {
			jsonRes += "[]";
		}
		//results
		jsonRes += ",\"total_count\":";
		if(!cnts[i].empty()){
			jsonRes += cnts[i];
		}else{
			jsonRes += "null";
		}
		jsonRes += "},";
	}
	//delete ,
	if(*jsonRes.rbegin() == ',')
		jsonRes.erase(jsonRes.size() - 1);
	jsonRes += "]";
}

/***********************************************************************
    @ File Name: QueryAutoFill.cpp
    @ Method:
    @ Author: Jerry Shi
    @ Mail: jerryshi0110@gmail.com 
    @ Created Time: 2015年10月08日 星期四 18时42分01秒
 ************************************************************************/
#include "QueryAutoFill.h"

QueryAutoFill::QueryAutoFill(const std::string& pinyin_res_pth,const std::string& dict_pth)
{
	if(!boost::filesystem::exists(pinyin_res_pth) 
		&& !boost::filesystem::exists(dict_pth) )
		std::cerr << "PinYin resource or token dictionary directory not exist!\n";

	idmlib::qc::CnQueryCorrection::res_dir_ = pinyin_res_pth.c_str();
	cngr_ = new idmlib::qc::CnQueryCorrection(pinyin_res_pth);
	cngr_->Load();//load pinyin.txt and trained files
	tok_ = new ilplib::knlp::HorseTokenize(dict_pth);

	//insert shengmu:
	//b,p,m,f,d,t,n,l,g,k,h,j,q,x,zh,ch,sh,r,z,c,s,y,w
	//
	ifstream fin;
	std::string Shm = pinyin_res_pth + "/ShengMu.txt";
	fin.open(Shm.c_str(),std::ifstream::in);
	if(!fin.is_open())
		std::cerr << "Open ShengMu file error!\n";
	std::string line;
	while(getline(fin,line))
	{
		if(line.length() == 0)
			continue;
		shm_.insert(line);
	}
	fin.close();
}

QueryAutoFill::~QueryAutoFill()
{
	if(tok_)
		delete tok_;
	if(cngr_)
		delete cngr_;
}

/**
 @QueryAutoFill::Parse
 @bref: Get every character and token terms from str
 **/
bool QueryAutoFill::Parse(const std::string& str
		,vector<std::string>& chars
		,vector<std::string>& words)
{
	izenelib::util::UString ustr(str,izenelib::util::UString::UTF_8);

	// get character
	chars.clear();
	for(unsigned int i = 0; i < ustr.length(); ++i) {
		izenelib::util::UString ch(1,ustr[i]);
		chars.push_back("");
		ch.convertString(chars.back(),izenelib::util::UString::UTF_8);
	}

	// get words
	words.clear();
	vector<std::pair<std::string,float> > terms;
	try {
		tok_->tokenize(str,terms);
	}
	catch(...) {
		terms.clear();
	}
	for(unsigned int i = 0; i < terms.size();++i){
		words.push_back(terms[i].first);
	}

	return true;
}

/**
 @ QueryAutoFill::Build
 @ bref: given a query file then generate it's autocompletation informations
 **/
bool QueryAutoFill::Build(const std::string& terms_file)
{
	terms_.clear();
	term_freq_.clear();
	key_termids_.clear();

	ifstream fin(terms_file.c_str());
	if(!fin)
		return false;
	std::cout << "Building...\n";	
	std::map<std::string,float> t_freq;
	std::string line;
	while(getline(fin,line)) {
		if(line.empty())
			continue;
		std::size_t pos = line.find("\t");
		std::string term;
		if(pos != std::string::npos)
			term = line.substr(0,pos);
		else
			continue;
		float freq = 0.0;
		try {
			freq = boost::lexical_cast<float>(line.substr(pos+1));
		} catch(...) {
			std::cout << "bad line in" << terms_file << ":" << line << std::endl;
			continue;
		}
		t_freq[term] = freq;
	}
//	std::cout << "1>>>Test file lines:" << t_freq.size() << std::endl;
	for(map<std::string,float>::iterator it = t_freq.begin();it != t_freq.end(); ++it) {
		terms_.push_back(it->first);
		term_freq_.push_back(it->second);
	}
	t_freq.clear();
//	std::cout << "1>>>Test file lines:" << terms_.size() << std::endl;
//	std::cout << "1>>>Test file lines:" << term_freq_.size() << std::endl;

	//generate informations for keyword in terms_file
	boost::unordered_map<std::string,vector<std::pair<int,float> > > key_info;
	for(unsigned int id = 0; id < terms_.size(); ++id) {
		//std::cout << "lines id:" << id << "\tterms:" << terms_[id] << std::endl;
		Generate(terms_[id],id,key_info);
	}
//	std::cout << "2>>>Generate ok!\n";
	// caculate score
	boost::unordered_map<std::string,vector<std::pair<int,float> > >::iterator it;
//	std::cout << "key_info size:" << key_info.size() << std::endl;

	for(it = key_info.begin(); it != key_info.end(); ++it) {
		vector<std::pair<int,float> >& info =  it->second;
		for(unsigned int id = 0; id < info.size(); ++id) {
			info[id].second = term_freq_[info[id].first] * info[id].second;
		}
		sort(info.begin(),info.end(),GreaterBySecond());

		//process results,dedup
		vector<int> termsid;
		set<int> uniq_termids;
		for(unsigned int id = 0; id < info.size() && uniq_termids.size() <=15;++id){
			if(uniq_termids.insert(info[id].first).second) {
				termsid.push_back(info[id].first);
			}
		}
		key_termids_[it->first].swap(termsid);
		uniq_termids.clear();
	}
	std::cout << "key_termids size:" << key_termids_.size() << std::endl;
	std::cout << "Completed building\n";	
}

/**
 @ QueryAutoFill::Flush
 @ bref: store generated data to files
 **/
bool QueryAutoFill::Flush(const std::string& terms_file,const std::string& key_terms_file)
{
	ofstream fout(terms_file.c_str());
	if(!fout)
		return false;
	ofstream fout2(key_terms_file.c_str());
	if(!fout2)
		return false;

	//save keywords in terms_file
	for(unsigned int i = 0;i < terms_.size(); ++i) {
		fout << i << "\t" << terms_[i] << "\n";
	}

	fout.close();

	boost::unordered_map<std::string,vector<int> >::iterator it;
	for(it = key_termids_.begin(); it != key_termids_.end(); ++it) {
		vector<int>& term_ids = it->second;
		//candicate is itself
		if(term_ids.size() == 1 && terms_[term_ids[0]] == it->first)
			continue;

		fout2 << it->first;
		for(unsigned int i = 0; i < term_ids.size(); ++i) {
			if(it->first == terms_[term_ids[i]])
				continue;
			fout2 << "\t" << terms_[term_ids[i]];
		}
		fout2 << "\n";
	}
	fout2.close();

	return true;
}

/**
 @ QueryAutoFill::Generate
 @ bref: generate informations(all the prefix) for input str based on the following method
 **/
bool QueryAutoFill::Generate(const std::string& term,const unsigned int& termid
		,boost::unordered_map<std::string,vector<std::pair<int,float> > >& key_info
		,const int len)
{
	if(term.empty())
		return true;

	vector<std::string> chars;
	vector<std::string> words;
	
	Parse(term,chars,words);

	vector<std::string> keys;
	GenerateByPrefix(chars,keys,len);
	for(unsigned int id = 0; id < keys.size();++id) {
		key_info[keys[id]].push_back(make_pair(termid,prefix_w));
	}
//	std::cout << "Gen1:" << term;
	
	GenerateByPinYinPrefix(term,keys,len);
	for(unsigned int id = 0; id < keys.size(); ++id) {
		key_info[keys[id]].push_back(make_pair(termid,pinyin_prefix_w));
	}
//	std::cout << "-Gen2:" << term;
	
	GenerateByShengMuPrefix(term,keys,len);
	for(unsigned int id = 0; id < keys.size(); ++id) {
		key_info[keys[id]].push_back(make_pair(termid,shengmu_w));
	}
//	std::cout << "-Gen3:" << term;

	GenerateByWordInfix(words,keys,len);
	for(unsigned int id = 0; id < keys.size(); ++id) {
		key_info[keys[id]].push_back(make_pair(termid,word_infix_w));
	}
//	std::cout << "-Gen4:" << term;

	GenerateByWordSuffix(words,keys,len);
	for(unsigned int id = 0; id < keys.size(); ++id) {
		key_info[keys[id]].push_back(make_pair(termid,word_suffix_w));
	}
//	std::cout << "-Gen5:" << term << std::endl;

	return true;

}

/**
 @ QueryAutoFill::GenerateByPrefix
 @ bref:generate keys based on every character,and param len is the maximum length of key
eg:chars = [a,b,c] ,then keys = [a,ab,abc]
 */
bool QueryAutoFill::GenerateByPrefix(const vector<std::string>& chars
		,vector<std::string>& keys,const int len)
{
	keys.clear();
	std::string prefix;
	for(unsigned int i = 0; i < chars.size() && i < len; ++i) {
		prefix += chars[i];
		keys.push_back(prefix);
	}
	return true;
}

/**
 @ QueryAutoFill::GenerateByWordInfix
 @ bref: generate keys based on infix in token terms,if token terms number < 3 
	then return directly,as there are only two terms so it's not necessay.
 **/
bool QueryAutoFill::GenerateByWordInfix(vector<std::string>& words
		,vector<std::string>& keys,const int len)
{
	keys.clear();
	unsigned int termSize = words.size();
	if(termSize < 3)
		return true;
	
	for(unsigned int i = 1;i < termSize - 1; ++i)
		keys.push_back(words[i]);
	return true;

}

/**
 @ QueryAutoFill::GenerateByWordsSuffix
 @ bref: generate keys by the last term
 **/
bool QueryAutoFill::GenerateByWordSuffix(vector<std::string>& words
		,vector<std::string>& keys,const int len)
{
	keys.clear();
	if(words.size() >  1)
		keys.push_back(words.back());
	return true;
}

/**
 @ QueryAutoFill::GenerateByPinYinPrefix
 @ bref: generate keys based on pinyin prefix which generated by GetPinyin2
 **/
bool QueryAutoFill::GenerateByPinYinPrefix(const std::string& str
		,vector<std::string>& keys,const int len)
{
	keys.clear();
	izenelib::util::UString ustr(str,izenelib::util::UString::UTF_8);
	vector<std::string> PinyinVec;
	cngr_->GetPinyin2(ustr,PinyinVec);

	set<std::string> prefixs;
	for(unsigned int i = 0; i < PinyinVec.size(); ++i) {
		std::string prefix;
		for(unsigned int j = 0; j < PinyinVec[i].size(); ++j) {
			prefix += PinyinVec[i][j]; // every letter
			if(prefixs.insert(prefix).second && prefix.length() < len)
				keys.push_back(prefix);
		}
	}
	return true;
}

/**
 @ QueryAutoFill::GenerateByShengMuPrefix
 @ bref: generate keys based on shengmu prefix of pinyin which generated by
	GetPinyin2
eg: 银行
	yin		y
	hang xing(polyphone) h x
	shengmu list:
	list[0]: y
	list[1]: h x	
 **/
bool QueryAutoFill::GenerateByShengMuPrefix(const std::string& str
		,vector<std::string>& keys,const int len)
{
	keys.clear();
	if(str.empty())
		return true;

	vector<vector<std::string> > shm_list;
	izenelib::util::UString ustr(str,izenelib::util::UString::UTF_8);
	for(unsigned int id = 0; id < ustr.length(); ++id) {
		izenelib::util::UString uchar;
		uchar += ustr[id]; // get a letter

		//specify the type and process
		// space
		if(uchar.isSpaceChar(0)) {
			shm_list.push_back(vector<std::string>(1," ")); // insert a space
			continue;
		}	
		else { // is chinese character
			if(uchar.isChineseChar(0)) {
			vector<std::string> shm;
			vector<std::string> pinyin;
			cngr_->GetPinyin2(uchar,pinyin); // extract pinyin
			GetShengMuByPinYin(pinyin,shm); // extract shegnmu from pinyin
			shm_list.push_back(vector<std::string>());
			shm_list.back().swap(shm);
		}
			else { // is alphabet/digital
				std::string unChineseChar;
				uchar.convertString(unChineseChar,izenelib::util::UString::UTF_8);
				shm_list.push_back(vector<std::string>(1,unChineseChar));
			}
	  }
	}

	//get all shengmu sequences,specify single/ multiple shengmu
	vector<std::string> shms = shm_list[0];
	for(unsigned int seq_id = 1; seq_id < shm_list.size(); ++seq_id) {
		vector<std::string>& cur_shm = shm_list[seq_id];

		if(cur_shm.size() == 1) {
			for(unsigned int id = 0; id < shms.size(); ++id) {
				shms[id] += cur_shm[0];
			}
		}
		else {
			vector<std::string> pre_shm;
			pre_shm.swap(shms);

			// combine the previous and current
			for(unsigned int pre_id = 0; pre_id < pre_shm.size(); ++pre_id) {
				for(unsigned int cur_id = 0; cur_id < cur_shm.size(); ++cur_id) {
					shms.push_back(pre_shm[pre_id] + cur_shm[cur_id]);
				}
			}
		}
	}

	//get all shengmu prefix
	set<std::string> prefixs;
	for(std::size_t id = 0; id < shms.size(); ++id) {
		std::string prefix;
		for(unsigned int chid = 0; chid < shms[id].size(); ++chid) {
			prefix += shms[id][chid];
			if(prefixs.insert(prefix).second && prefix.length() < len)
				keys.push_back(prefix);
		}
	}
	
	return true;
}

/**
 @ QueryAutoFill::GetShengMuByPinYin
 @ bref: extract shengmu from pinyin 
	eg: 行 hang,xing shengmu: h,x
 **/
void QueryAutoFill::GetShengMuByPinYin(const vector<std::string>& pinyin
		,vector<std::string>& shm)
{
	shm.clear();
	set<std::string> head;
	for(unsigned int i = 0; i < pinyin.size(); ++i) {
		const std::string& py = pinyin[i];
		if(py.empty())
			continue;
		else {
			std::string ch;
			ch += py[0]; //first char
			if((char)(ch[0]) >= 0) {   // 
				if(head.insert(ch).second)
					shm.push_back(ch);
			}
			else {
				if(head.insert(py).second)
					shm.push_back(py);
			}
		}
	}
}

/*************************************************************************
    @ File Name: common.h
    @ Method: This file contain headers of C/C++ or other library 
    @ Author: Jerry Shi
    @ Mail: jerryshi0110@gmail.com 
    @ Created Time: 2015年10月08日 星期四 13时33分48秒
 ************************************************************************/
#ifndef COMMON_H
#define COMMON_H

#include <iostream>
#include <vector>
#include <string>
#include <set>
#include <map>

#include <algorithm>
#include <fstream>
#include <sstream>

//boost
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/unordered_map.hpp>
#include <boost/concept_check.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/filesystem.hpp>


#include <glog/logging.h>
//
//#include "knlp/horse_tokenize.h"
//#include "idmlib/query-correction/cn_query_correction.h"


//types defination

#endif // common.h


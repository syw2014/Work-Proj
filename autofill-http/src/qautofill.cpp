/*************************************************************************
    @ File Name: qautofill.cpp
    @ Method:
    @ Author: Jerry Shi
    @ Mail: jerryshi0110@gmail.com 
    @ Created Time: 2015年10月23日 星期五 10时28分11秒
 ************************************************************************/
#include <iostream>
#include <sys/types.h>
#include <signal.h>

#include "QueryIndex.h"
#include "mongoose.h"
#include "common.h"

//
std::string res_pth = "/work/mproj/workproj/github/token-dict/nlp-corpus/queryLog/autofill-res";
QueryIndex qs(res_pth);

static int ev_handler(struct mg_connection *conn, enum mg_event ev){
	
	boost::posix_time::ptime time_start,time_end;
	boost::posix_time::millisec_posix_time_system_config::time_duration_type time_elapse;

	std::string key;
	std::string JsonRes = "[]";
	switch(ev){
		case MG_AUTH: return MG_TRUE;
		case MG_REQUEST:
		{
			bool succ = true;
			char buffer[65535];
			time_start = boost::posix_time::microsec_clock::universal_time();
			
			try{
				//get
				if(mg_get_var(conn,"key",buffer,sizeof(buffer)) > 0){
					std::string pvalue(buffer);

					key = pvalue;
				}
				//post
				else if(strcmp(conn->request_method,"POST") == 0){
					std::string content(conn->content,conn->content_len);
					key = content;
				}
			} catch(std::exception& ex){
				LOG(ERROR) << ex.what();
				//std::cerr << ex.what() << std::endl;
				succ = false;
			}

			//process request data
			time_end = boost::posix_time::microsec_clock::universal_time();
			time_elapse = time_end - time_start;
			int request_time = time_elapse.ticks();
			
			//get results
			if(succ && !key.empty()){
				qs.GetJsonRes(key,JsonRes);
			}
			time_end = boost::posix_time::microsec_clock::universal_time();
			time_elapse = time_end - time_start;
			int total_time = time_elapse.ticks();

			/*std::cout << "Request Time:" << request_time << "\tTotal Time:"
				<< total_time << "\tKey:" << key << "\tResults:"
				<< JsonRes << '\n';*/
			LOG(INFO) << "Request Time: " << request_time << "us\tTotal Time:"
				<< total_time << "us\tKey:" << key << "\t Results:"
				<< JsonRes;

			mg_send_data(conn,JsonRes.c_str(),JsonRes.length());
			//mg_printf_data(conn,JsonRes.c_str(),JsonRes.length());
			return MG_TRUE;
		}
		default: return MG_FALSE;
	}
}

static void *serve(void *server){
	for(;;) mg_poll_server((struct mg_server *)server, 1000);
	return NULL;
}

void start_http_server(int pool_size = 1){

	std::vector<struct mg_server*> servers;
	for(int p = 0; p < pool_size; ++p){
		std::string name = boost::lexical_cast<std::string>(p);
		LOG(INFO) << "Create thread id :" << name;
		//std::cout << "Creat thread: " << name << std::endl;
		struct mg_server * server;
		server = mg_create_server((void*)name.c_str(), ev_handler);
		if(p == 0){
			mg_set_option(server,"listening_port", "18665");
		}else{
			mg_copy_listeners(servers[0], server);
		}

		servers.push_back(server);
	}
	for(uint32_t p = 0; p < servers.size(); ++p){
		struct mg_server* server = servers[p];
		mg_start_thread(serve,server);
	}
	sleep(3600*24*365*10);
}

int main(int argc , char* argv[])
{
	int thread_num = 22;
	std::cout << "Start program...\n";
	while(1)
	{
		start_http_server(thread_num);
	}

	return EXIT_SUCCESS;
}

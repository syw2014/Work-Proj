/*************************************************************************
    @ File Name: InsertrsKeywords.cpp
    @ Method: insert related suggested keywords to recommendation by request
    @ Author: Jerry Shi
    @ Mail: jerryshi0110@gmail.com 
    @ Created Time: 2015年09月16日 星期三 16时14分51秒
 ************************************************************************/
#include <iostream>
#include <curl/curl.h>
#include <vector>
#include <string>
#include <fstream>
#include <boost/algorithm/string.hpp>
#include "json/json.h"

using namespace std;
string gData;
size_t ResponseData(char* buf,size_t size,size_t nmemb,void* up)
{
	for(int i = 0; i < size * nmemb; ++i)
	{
		gData.push_back(buf[i]);
	}

	return size*nmemb;
}

//post request
bool postRequest(string& str)
{
	CURL *curl;
	CURLcode res;
	curl = curl_easy_init();
	if(curl)
	{
		//post data
		curl_easy_setopt(curl,CURLOPT_POSTFIELDS,str.c_str()); 
		//curl_easy_setopt(curl,CURLOPT_URL,"http://10.0.2.15:18663/put");
		curl_easy_setopt(curl,CURLOPT_URL,"http://10.30.97.23:18663/put");

		res = curl_easy_perform(curl);
		curl_easy_cleanup(curl);
	}

	return true;
}

int main(int argc ,char** argv)
{
	if(argc != 2)
	{
		cout << "Usage: " << argv[0] << "\t No source file\n";
		return -1;
	}

	string File = argv[1];
	vector<string> v;
	Json::Value json;
	Json::Value rskeywords;
	ifstream in;
	in.open(File.c_str());
	if(!in.is_open())
		cerr << "Open source file error!\n";
	string line;
	while(getline(in,line))
	{
		if(line.size() == 0)
			continue;
		v.clear();
		json.clear();
		rskeywords.clear();

		cout << "PostData:" << line << endl;
		boost::split(v,line,boost::is_any_of("\t"));
		if(v.size() < 2)
			continue;
		json["query"] = v[0];
		for(int i = 1; i < v.size(); ++i)
		{
			if(v[i].size() != 0)
				rskeywords.append(v[i]);
		}
		json["rskeywords"] = rskeywords;
		string post = json.toStyledString();
		postRequest(post);
		//sleep(10);
	}

	cout << "process done!\n";
	return 0;
}

//complie commends:
//g++ filename -o filename -I ../libs/jsoncpp/include/ -L ../libs/jsoncpp/lib -lcurl -ljson -lboost_system
